#Plantilla para Informes de Práctica, Departamento de Industrias, USM

Plantilla básica (en LaTeX y LyX) para los Informes de Prácticas (Industriales y Profesionales) del [Departamento de Industrias](http://www.industrias.usm.cl) de la [Universidad Técnica Federico Santa María](http://www.usm.cl/), Chile.


##Requisitos

Esta plantilla está disponible en dos versiones:

* LaTeX
* LyX

y para ocupares se requiere:

* Una versión reciente de LaTeX (2014 o posterior).
* El editor LyX (LyX-2.1.2 o posterior, sólo para plantilla *.lyx).

##Uso

###LaTeX

Para compilar el código latex:

    $ pdflatex informe.tex
    $ bibtex internship
    $ pdflatex informe.tex
    $ pdflatex informe.tex
    
NOTA: `pdflatex` debe ejecutarse (en consola) tres veces, como se indica y en el orden mostrado para que latex pueda construir las Tablas de Contenidos y las referencias cruzadas de la Bibliografía.

###LyX

La plantilla en LyX se encuentra en la carpeta del mismo nombre.

Para usar la plantilla en LyX, sólo debe editar los archivos que desee modificar. No es necesario editar el documento mastro (`informe.lyx`).

Para compilar, abra este documento maestro (`informe.lyx`) y podrá compilarlo en el Menú 'Ver’ -> 'Ver [PDF (pdflatex)] (CRTL+r ó CMD+r en Mac).

##Descargas

* [Release v1.4.1](https://bitbucket.org/jaime_rcz/usm-internship/get/1.4.1.zip) (2015-11-05)
* [Última Versión Estable](https://bitbucket.org/jaime_rcz/usm-internship/get/master.zip) (2015-11-05)
* [Versión en desarrollo](https://bitbucket.org/jaime_rcz/usm-internship/get/develop.zip)



##Plataformas Soportadas

Esta plantilla es independiente de la plataforma empleada (Windows, Mac o Linux).

**Nota :** la plantilla en LaTeX fue escrita usando una codificación de caracteres UTF-8, la que en ocasiones puede general conflictos en Windows cuando se agregan archivos con codificación distinta. Windows ocupa por defecto una codificación no compatible con UTF-8.

##Documentación
Revise los archivos de salida (`informe.pdf`) de cada plantilla, LaTeX o Lyx, para más información.

Los archivos maestros (`informe.tex` e `informe.lyx`) contienen información sobre como modificar parámetros básicos.

##Contribuciones
Dirigirlas directamente al autor.

##Licencia
Educational Community License version 1.0 .

Las imágenes son propiedad intelectual de la UTFSM, y están sujetas a leyes chilenas e internacionales de protección a la propieded intelectual. Sólo para uso en documentos oficiales de la UTFSM.